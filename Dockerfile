FROM ubuntu:latest
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install apt-utils
RUN apt-get -y install apache2
RUN apt-get -y install php5.5
RUN service apache2 restart
RUN rm /var/www/html/index.html
COPY index.php /var/www/html/
#ENTRYPOINT ["/usr/sbin/apache2", "-k", "start"]
#ENV APACHE_RUN_USER www-data
#ENV APACHE_RUN_GROUP www-data
#ENV APACHE_LOG_DIR /var/log/apache2
EXPOSE 80
CMD apachectl -D FOREGROUND
